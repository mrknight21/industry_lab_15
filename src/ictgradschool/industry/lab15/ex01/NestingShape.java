package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mche618 on 9/05/2017.
 */
public class NestingShape extends Shape {

    private List<Shape> children = new ArrayList<Shape>();


    public NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }


    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }


    @Override
    public void move(int width, int height) {


        int nextX = fX + fDeltaX;
        int nextY = fY + fDeltaY;

        if (nextX <= 0) {
            nextX = 0;
            fDeltaX = -fDeltaX;
        } else if (nextX + fWidth >= width) {
            nextX = width - fWidth;
            fDeltaX = -fDeltaX;
        }

        if (nextY <= 0) {
            nextY = 0;
            fDeltaY = -fDeltaY;
        } else if (nextY + fHeight >= height) {
            nextY = height - fHeight;
            fDeltaY = -fDeltaY;
        }

        fX = nextX;
        fY = nextY;

        for (Shape child : children) {
            child.move(this.getWidth(), this.getHeight());
        }

    }

    @Override
    public void paint(Painter painter) {
        painter.drawRect(fX, fY, fWidth, fHeight);
        painter.translate(fX, fY);
        for (Shape child : children) {
                child.paint(painter);
            }
        painter.translate(-fX, -fY);
    }



    public void add(Shape child) throws IllegalArgumentException {

		/* Check first that the file argument does not yet have a parent. */
        if (child.parent() != null) {
            throw new IllegalArgumentException("This Shape already has parent");
        }

		/* Check that the file to be added is not the root node. */
        List<Shape> fullPath = this.path();
        Shape root = fullPath.get(0);
        if (root == child) {
            throw new IllegalArgumentException("Attempted to add shape that is the root node");
        }

        if( child.fX+child.fWidth > this.fWidth || child.fY+child.fHeight > this.fHeight){
            throw new IllegalArgumentException("Attempted to add shape that is out of bound");
        }

        children.add(child);
        child.setParent(this);
    }

    public boolean remove(Shape child) {
        boolean result = false;
        if (children.remove(child)) {
            result = true;
        }
        ;
        child.setParent(null);
        return result;
    }


    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > (children.size() - 1)) {
            throw new IndexOutOfBoundsException("Invalid index input. Index should be either bigger/equal than 0 or smaller than " + children.size());
        }
        return children.get(index);
    }

    public int shapeCount() {
        return children.size();
    }

    public int indexOf(Shape child) {
        return children.indexOf(child);
    }

    public boolean contains(Shape child) {
        return children.contains(child);
    }


}
