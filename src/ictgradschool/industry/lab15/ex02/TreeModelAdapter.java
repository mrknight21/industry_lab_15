package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.*;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Created by mche618 on 9/05/2017.
 */
public class TreeModelAdapter implements TreeModel{

    private Shape root;
    private List<TreeModelListener> treeModelListeners;


    public TreeModelAdapter(Shape root) {
        this.root = root;
        this.treeModelListeners = new ArrayList<TreeModelListener>();
    }


    @Override
    public Object getRoot() {
        return root;
    }


    public int getChildCount(Object parent) {
        int result = 0;
        if (parent instanceof NestingShape) {
             NestingShape par = (NestingShape) parent;
            result = par.shapeCount();
        }
        return result;
    }


    public Object getChild(Object parent, int index) {
        Shape result = null;

        if (parent instanceof NestingShape) {
            NestingShape par = (NestingShape) parent;
            result = par.shapeAt(index);
        }
        return result;
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
    }


    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;
        if (parent instanceof NestingShape) {
            NestingShape par = (NestingShape) parent;
            boolean found = false;
            for ( int i = 0; i < par.shapeCount() && !found; i ++) {

                 Shape current = par.shapeAt(i);
                if (child == current) {
                    found = true;
                    indexOfChild = i;
                }
            }
        }
        return indexOfChild;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.remove(l);
    }


}
